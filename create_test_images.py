import numpy as np
import cv2


def draw_circle(image, position, radius, color):
    shift = 8
    scale = 1 << shift
    return cv2.circle(image, (int(scale*position[0]), int(scale*position[1])), int(scale*radius), color, lineType=cv2.LINE_AA, thickness=-1, shift=shift)


def get_image(image_size, circle_radius, hole_radius, centre, angle):
    image = np.zeros((image_size, image_size), np.uint8)
    image = draw_circle(image, centre, circle_radius, 255)
    hole_centre = centre + 0.5*circle_radius * np.array([np.cos(np.radians(angle)), np.sin(np.radians(angle))])
    image = draw_circle(image, hole_centre, hole_radius, 0)
    noise_image = np.random.random((image_size, image_size))
    noise_ratio = 0.05
    image = (255*((1-noise_ratio)*image/255. + noise_ratio*noise_image)).astype(np.uint8)
    return image


def create_test_image(index, angle):
    image = get_image(32, 12, 4, np.array([15.5, 15.5]), angle)
    cv2.imwrite("test_images/" + str(index) + ".png", image)


num_images = 10
angles = [np.random.randint(360) for _ in range(num_images)]
np.savetxt("test_images/angles.txt", angles, fmt='%i')
for i in range(num_images):
    create_test_image(i, np.random.randint(360))
