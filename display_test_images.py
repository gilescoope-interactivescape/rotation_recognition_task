import cv2
import numpy as np

angles = np.loadtxt("test_images/angles.txt").astype(int)
for i in range(10):
    image = cv2.imread("test_images/" + str(i) + ".png")
    vis = cv2.resize(image, (128, 128), interpolation=cv2.INTER_NEAREST)
    cv2.imshow("vis", vis)
    print("showing image", i, "angle", angles[i])
    cv2.waitKey(-1)
